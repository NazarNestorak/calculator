package com.epam;

import java.util.Scanner;

public class Calculator {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        double num1 = getInt();
        double num2 = getInt();
        char operation = getOperation();
        double result = calc(num1,num2,operation);
        System.out.println("Operation result: "+result);
    }

    public static double getInt(){
        System.out.println("Enter number:");
        double num;
        if(scanner.hasNextInt()){
            num = scanner.nextInt();
        } else {
            System.out.println("You made mistake. Try again.");
            scanner.next();
            num = getInt();
        }
        return num;
    }

    public static char getOperation(){
        System.out.println("Enter operation:");
        char operation;
        if(scanner.hasNext()){
            operation = scanner.next().charAt(0);
        } else {
            System.out.println("You made mistake when entered operation. Try again.");
            scanner.next();
            operation = getOperation();
        }
        return operation;
    }

    public static double calc(double num1, double num2, char operation){
        double result;
        switch (operation){
            case '+':
                result = num1+num2;
                break;
            case '-':
                result = num1-num2;
                break;
            case '*':
                result = num1*num2;
                break;
            case '/':
                result = divide(num1,num2);
                break;
            default:
                System.out.println("Unknown operation . Tru again.");
                result = calc(num1, num2, getOperation());
        }
        return result;
    }

    public static double divide(double first, double second) {
        double result = 0;
        try {
            result = first / second;
        } catch (ArithmeticException e) {
            System.out.println("you can`t divide by zero!");
        }
        return result;
    }
}
