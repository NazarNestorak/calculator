package com.epam;

import org.junit.jupiter.api.Test;
import static org.junit.Assert.assertTrue;

public class CalculatorTest {

    @Test
    public void testPlus() {
        Calculator calculator = new Calculator();
        double result = calculator.calc(1, 1, '+');
        assertTrue(2==result);
    }

    @Test
    public void testMinus() {
        Calculator calculator = new Calculator();
        double result = calculator.calc(3, 2, '-');
        assertTrue(1==result);
    }

    @Test
    public void testMultiply() {
        Calculator calculator = new Calculator();
        double result = calculator.calc(3, 2, '*');
        assertTrue(6==result);
    }

    @Test
    public void testDivide() {
        Calculator calculator = new Calculator();
        double result = calculator.calc(6, 2, '/');
        assertTrue(3==result);
    }
}
